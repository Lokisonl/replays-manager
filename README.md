﻿[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_replays-manager&metric=alert_status)](https://sonarcloud.io/dashboard?id=wot-public-mods_replays-manager)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_replays-manager&metric=ncloc)](https://sonarcloud.io/dashboard?id=wot-public-mods_replays-manager)
[![Visits](https://gitlab.poliroid.ru/api/badge/replays-manager/visits)](https://gitlab.com/wot-public-mods/replays-manager)
[![Downloads](https://gitlab.poliroid.ru/api/badge/replays-manager/downloads)](https://gitlab.com/wot-public-mods/replays-manager/-/releases)
[![Donate](https://cdn.poliroid.ru/gitlab/donate.svg)](https://poliroid.ru/donate)

**Replays Manager** This is a modification for the game "World Of Tanks" which allows you to convenient view replays, results, playback, and uploading replays to wotreplays site

### An example of what a manager looks like
![An example of what a manager looks like](https://static.poliroid.ru/replaysManager.jpg)

